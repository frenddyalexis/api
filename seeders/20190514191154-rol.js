'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    
      return queryInterface.bulkInsert('Roles', [
      {
        name: 'USER',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        name: 'ADMIN',
        createdAt: new Date(),
        updatedAt: new Date()
      },
       {
        name: 'PM',
        createdAt: new Date(),
        updatedAt: new Date()
      }
      ], {});
    
  },

  down: (queryInterface, Sequelize) => {
  
      return queryInterface.bulkDelete('Roles', null, {});
  
  }
};
