const secret = process.env.SECRET;

module.exports = {
  'secret': secret,
  ROLEs: ['USER', 'ADMIN', 'PM']
};