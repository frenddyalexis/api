const mysqlhost = process.env.MYSQLHOST;
const mysqluser= process.env.MYSQLUSER;
const mysqlpassword = process.env.MYSQLPASSWORD;
const mysqldatabase = process.env.MYSQLDATABASE;
const dialect = process.env.DIALECT;

const pool_max = parseInt(process.env.POOL_MAX);
const pool_min = parseInt(process.env.POOL_MIN);
const pool_acquire = process.env.POOL_ACQUIRE;
const pool_idle = process.env.POOL_IDLE;
 
const Sequelize = require('sequelize');
const sequelize = new Sequelize(`${mysqldatabase}`, `${mysqluser}`, `${mysqlpassword}`, {
  host: `${mysqlhost}`,
  dialect: `${dialect}`,
  define: {
    underscored: false,
    freezeTableName: false,
    charset: 'utf8',
    dialectOptions: {
      collate: 'utf8_general_ci'
    },
    timestamps: true
  },
  pool: {
    max: pool_max,
    min: pool_min,
    acquire: pool_acquire,
    idle: pool_idle
  }
});
 
const db = {};
 
db.Sequelize = Sequelize;
db.sequelize = sequelize;
 

 

module.exports = db;