const db = require('../models');
const config = require('../config/role.config.js');
const User = db.User;
const Role = db.Role;
const User_role = db.User_role;

const Op = db.Sequelize.Op;

var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
// Generate a salt
var salt = bcrypt.genSaltSync(10);

exports.signup = (req, res) => {
    // Save User to Database
    console.log("Processing func -> SignUp");


    User.create({
        name: req.body.name,
        username: req.body.username,
        email: req.body.email,
        password: bcrypt.hashSync(req.body.password, salt)
    }).then(user => {
        Role.findAll({
          where: {
            id: {
              [Op.or]: req.body.roles
            }
          }
        }).then(roles => {
            console.log(roles)
            user.setRoles(roles).then(() => {
                res.send("User registered successfully!");
            });
        }).catch(err => {
            res.status(500).send("Error -> " + err);
        });
    }).catch(err => {
        res.status(500).send("Fail! Error -> " + err);
    })
}


exports.signin = (req, res) => {
    console.log("Sign-In");
    
    User.findOne({
        where: {
            username: req.body.username
        }
    }).then(user => {
        if (!user) {
            return res.status(404).send('User Not Found.');
        }

        var passwordIsValid = bcrypt.compareSync(req.body.password, user.password);
        if (!passwordIsValid) {
            return res.status(401).send({ auth: false, accessToken: null, reason: "Invalid Password!" });
        }
        
        var token = jwt.sign({id: user.id }, config.secret, {
          expiresIn: 86400 // expires in 24 hours
        });
        
        res.status(200).send({ auth: true, accessToken: token });
        
    }).catch(err => {
        res.status(500).send('Error -> ' + err);
    });
}


exports.getUser = (req, res) => {
    console.log('elid:',req.userId);
    User.findOne({
        where: {id: req.params.id},
        attributes: ['name', 'username', 'email'],
         include: [{
            model: Role,
            attributes: ['id', 'name'],
            through: {
                attributes: ['userId', 'roleId'],
            }
        }],
        logging: console.log
    }).then(user => {
        console.log(user);
        res.status(200).json({
            "description": "User Info",
            "user": (user) ? user : 'Usuario no existe'
        });
    }).catch(err => {
        res.status(500).json({
            "description": "Can not access User Info",
            "error": err
        });
    })
}


exports.adminBoard = (req, res) => {
    User.findOne({
        where: {id: req.userId},
        attributes: ['name', 'username', 'email'],
        include: [{
            model: Role,
            attributes: ['id', 'name'],
            through: {
                attributes: ['userId', 'roleId'],
            }
        }]
    }).then(user => {
        res.status(200).json({
            "description": "Admin Board",
            "user": user
        });
    }).catch(err => {
        res.status(500).json({
            "description": "Can not access Admin Board",
            "error": err
        });
    })
}













// var UserModel = require('../models/users.model');

// var jwt = require('jsonwebtoken');
// var bodyParser = require('body-parser');



// exports.getUsers = function (req, res) {
//     console.log("usuariooos");
//     UserModel.getUsers(function (error, data) {
//         res.status(200).json(data);
//     });
// };

// exports.getUser = function (req, res) {
//     console.log("dsdsd");
//     var id = req.params.id;
//     console.log(id);
//     UserModel.getUser(id, function (error, data) {
//         if (typeof data !== 'undefined' && data.length > 0) {
//             res.status(200).json(data);
//         }
//         else {
//             res.status(200).json({ "mensaje": "Usuario no existe" });
//         }
//     });
// };

// exports.updateActive = function (req, res) {
//     var userData = { id: req.body.id, active: req.body.active };
//     console.log(userData);

//     UserModel.updateActive(userData, function (error, data) {

//         if (data && data.msg) {
//             res.status(200).json(data);
//         }
//         else {
//             res.status(200).json({ "mensaje": "Error" });
//         }
//     });
// };



// exports.addUser = function (req, res) {
//     var userData = {
//         id: null,
//         username: req.body.username,
//         email: req.body.email,
//         password: req.body.password,
//         active: 1
//     };
//     UserModel.insertUser(userData, function (error, data) {
        
//         if (data && data.insertId) {
//             res.redirect("/users/" + data.insertId);
//         }
//         else {
//             res.json(500, { "mensaje": "Error" });
//         }
//     });
// };


// exports.deleteUser = function (req, res) {
//     var id = req.param('id');
//     UserModel.deleteUser(id, function (error, data) {
//         if (data && data.msg === "deleted" || data.msg === "notExist") {
//             res.json(200, data);
//         }
//         else {
//             res.json(500, { "mensaje": "Error" });
//         }
//     });
// };

// exports.login = function (req, res){
//   var username = req.body.user;
//   var password = req.body.password;


//  console.log(req.body);
  
//     console.log(req.body);
//     // UserModel.login(id, function (error, data) {
//     //     if (typeof data !== 'undefined' && data.length > 0) {
//     //         res.status(200).json(data);
//     //     }
//     //     else {
//     //         res.status(200).json({ "mensaje": "Usuario no existe" });
//     //     }
//     // });
  



 
//   if( !(username === 'oscar' && password === '1234')){
//     res.status(401).send({
//       error: 'usuario o contraseña inválidos'
//     })
//     return
//   }
 
//   var tokenData = {
//     username: username
//     // ANY DATA
//   }
 
//   var token = jwt.sign(tokenData, 'Secret Password', {
//      expiresIn: 60 * 60 * 24 // expires in 24 hours
//   })
 
//   res.send({
//     token
//   })
// }