module.exports = (sequelize, DataTypes) => {
  const Role = sequelize.define('Role', {
	  name: {
		  type: DataTypes.STRING
	  }
  }, {});
  Role.associate = function(models) {
      Role.belongsToMany(models.User,{

      through: 'user_roles',
      foreignKey: 'roleId',
      otherKey: 'userId'

      });
  };

  return Role;
};

