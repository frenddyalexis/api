module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
     name: {
		  type: DataTypes.STRING
	  },
	  username: {
		  type: DataTypes.STRING
	  },
	  email: {
		  type: DataTypes.STRING
	  },
	  password: {
		  type: DataTypes.STRING
	  }
  }, {});


  User.associate = models =>  {
  User.belongsToMany(models.Role,{
	  	foreignKey: 'userId',
	  	otherKey:'roleId',
	  	through: 'user_roles'
	  	});
  };
  return User;
};




