const express = require('express');
const router = express.Router();
const controller = require('../controllers/user.controller.js');


const validationUser = require('../validator/user.validator');
const commonValidator = require('../validator/common.validator');

const { check, validationResult } = require('express-validator/check');
const { body } = require('express-validator/check');

router.post('/signup', function (req, res, next) {
    res.status(200).send("localhost:3000/api");
});


router.post('/signup', [
	check('username').exists().custom(val => validationUser.checkExist(val,'username')),
	check('email').exists().isEmail().custom(val => validationUser.checkExist(val,'email')),
	check('password').isLength({ min: 5 })
	], (req, res) =>{
    // Finds the validation errors in this request and wraps them in an object with handy functions
    console.log(req.body);


    if (!req.body.rol) {
       console.log(req.body.rol);
    }

    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
    }



    controller.signup(req, res);
})
  // Finds the validation errors in this request and wraps them in an object with handy functions

router.post('/signin', controller.signin);


;

module.exports = router;




