// routes/index.js

// back in our API router
const express = require('express');
const router = express.Router();



const auth = require('./auth.routes.js');
const user = require('./user.routes.js');

// mount our 'auth' router onto the API router
// api/auth/
router.use('/auth', auth); 

// let's mount a few more...
router.use('/users', user);  


router.post('/', function (req, res, next) {
    res.status(200).send("localhost:3000/api/");
});


module.exports = router;  