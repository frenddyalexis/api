const express = require('express');
const router = express.Router();

const controller = require('../controllers/user.controller.js');
module.exports = router;

const validationUser = require('../validator/user.validator');
const commonValidator = require('../validator/common.validator');

const { check, validationResult } = require('express-validator/check');
const { body } = require('express-validator/check');

router.post('/', function (req, res, next) {
    res.status(200).send("localhost:3000/api/user");
});


// //Listar todos los usuarios
// router.get("/users", controller.getUsers );

//Listar usuario por id
router.get('/:id', [
    commonValidator.verifyToken
    ], (req, res) =>{
    console.log(req.params);
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
    }
    controller.getUser(req, res);
})











//Activar o desactivar usuario
// router.put("/users", controller.updateActive );

// //agregar usuario
// router.post("/users", controller.addUser );

// //Eliminar usuario
// router.delete("/users", controller.deleteUser );