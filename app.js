require('dotenv').config()

const port = process.env.APP_PORT || 8080;
const host = process.env.APP_HOST || '127.0.0.1';

const app = require('express')();
const http = require('http').Server(app);
const io = require('socket.io')(http);
var jwt = require('jsonwebtoken');

const Sequelize = require('sequelize');
let sequelize;


const model= require('./models');
model.sequelize.sync().then(function(){
  console.log('DATABASE CONNECT');
}).catch(function(err){
  console.log(err);
});




const router = require('./routes/');
// const db = require('./config/db.config.js');
// const Role = db.role;





const bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

const commonValidator = require('./validator/common.validator');

app.get('/', function(req, res){
   console.log('Hola Mundo');
});

io.on('connection', function(socket){
  socket.broadcast.emit('user connected');
  console.log('an user connected');
  console.log(socket.id);
  socket.on('chat message servidor', function(msg){
  	console.log('llegoalserver');
    io.emit('chat message cliente', msg);
  });
  socket.on('disconnect', function() {
      console.log('Got disconnect!');
 	  console.log(socket.id);
   });
});

http.listen(port, function(){
  console.log('listening on *:' + port);
});

app.use('/api', router);

// force: true will drop the table if it already exists
// model.sequelize.sync({force: true}).then(() => {
//   console.log('Drop and Resync with { force: true }');
//   initial();
// });


io.use((socket, next) => {
if (socket.handshake.query && socket.handshake.query.token){
    jwt.verify(socket.handshake.query.token, 'grokonez-super-secret-key', function(err, decoded) {
    	console.log(err);
      if(err) return next(new Error('Authentication error'));
      socket.decoded = decoded;
      console.log(socket.decoded);
      next();
    });
  } else {
      next(new Error('Authentication error'));
  }    
});


// function initial(){
// 	model.Role.create({
// 		name: "USER"
// 	});
	
// 	model.Role.create({
// 		name: "ADMIN"
// 	});
	
// 	model.Role.create({
// 		name: "PM"
// 	});
// }

console.log(`Ejecutando servidor en ${host}:${port}`);