const db = require('../models');
const config = require('../config/role.config.js');
const bodyParser = require('body-parser');
const ROLEs = config.ROLEs;
const User = db.User;
const Role = db.role;
const express = require("express");
const app = express();



app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());





checkExist = (userOrEmail, field)=>{
      return User.findOne({
             where: db.sequelize.where(db.sequelize.col(field), userOrEmail),
             logging: console.log
        }).then(result => {
        if (result) throw new Error('Exist')
      })
}

checkRolesExisted = (req, res, next) => {
	console.log(req.body);
	console.log(req.body.roles);
    for (let i = 0; i < req.body.roles.length; i++) {
        if (!ROLEs.includes(req.body.roles[i].toUpperCase())) {
            res.status(400).send("Fail -> Does NOT exist Role = " + req.body.roles[i]);
            return;
        }
    }
    next();
}

const signUpVerify = {};
signUpVerify.checkExist = checkExist;
signUpVerify.checkRolesExisted = checkRolesExisted;

module.exports = signUpVerify;